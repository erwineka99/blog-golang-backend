/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.6.51 : Database - database_blog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`database_blog` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `database_blog`;

/*Table structure for table `article_categories` */

DROP TABLE IF EXISTS `article_categories`;

CREATE TABLE `article_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext,
  `description` longtext,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_categories_user` (`user_id`),
  CONSTRAINT `fk_article_categories_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `article_categories` */

insert  into `article_categories`(`id`,`name`,`description`,`created_at`,`updated_at`,`user_id`) values 
(1,'makanan','makanan deskripsi','2021-12-25 08:43:22.757','2021-12-25 08:43:22.757',1),
(2,'resep','resep deskripsi','2021-12-25 08:43:41.686','2021-12-25 08:43:41.686',1);

/*Table structure for table `article_comments` */

DROP TABLE IF EXISTS `article_comments`;

CREATE TABLE `article_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment` longtext,
  `parent_comment_id` bigint(20) unsigned DEFAULT NULL,
  `article_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_comments_user` (`user_id`),
  KEY `fk_article_comments_article` (`article_id`),
  CONSTRAINT `fk_article_comments_article` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `fk_article_comments_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `article_comments` */

/*Table structure for table `article_logs` */

DROP TABLE IF EXISTS `article_logs`;

CREATE TABLE `article_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `action` longtext,
  `article_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `log_at` datetime(3) DEFAULT NULL,
  `old_article` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_article_logs_user` (`user_id`),
  KEY `fk_article_logs_article` (`article_id`),
  CONSTRAINT `fk_article_logs_article` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `fk_article_logs_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `article_logs` */

insert  into `article_logs`(`id`,`action`,`article_id`,`user_id`,`log_at`,`old_article`) values 
(1,'Update Article',1,1,'2021-12-25 09:59:39.971','{\"id\":1,\"title\":\"Article Title 1\",\"content\":\"Lorem Ipsum Updated\",\"articleCategoryID\":1,\"userID\":1,\"created_at\":\"2021-12-25T09:19:57.493+07:00\",\"updated_at\":\"2021-12-25T09:59:39.964+07:00\"}');

/*Table structure for table `article_ratings` */

DROP TABLE IF EXISTS `article_ratings`;

CREATE TABLE `article_ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rating` double DEFAULT NULL,
  `rating_name` longtext,
  `article_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_ratings_user` (`user_id`),
  KEY `fk_article_ratings_article` (`article_id`),
  CONSTRAINT `fk_article_ratings_article` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `fk_article_ratings_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `article_ratings` */

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` longtext,
  `content` longtext,
  `article_category_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_article_categories_article` (`article_category_id`),
  KEY `fk_articles_user` (`user_id`),
  CONSTRAINT `fk_article_categories_article` FOREIGN KEY (`article_category_id`) REFERENCES `article_categories` (`id`),
  CONSTRAINT `fk_articles_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `articles` */

insert  into `articles`(`id`,`title`,`content`,`article_category_id`,`created_at`,`updated_at`,`user_id`) values 
(1,'Article Title 1','Lorem Ipsum Updated',1,'2021-12-25 09:19:57.493','2021-12-25 09:59:39.964',1),
(3,'Article Title 2','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',1,'2021-12-25 09:58:31.730','2021-12-25 09:58:31.730',1),
(4,'Article Title 3','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',1,'2021-12-25 09:58:37.638','2021-12-25 09:58:37.638',1),
(5,'Article Title 4','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',2,'2021-12-25 09:58:58.374','2021-12-25 09:58:58.374',1),
(6,'Article Title 5','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',2,'2021-12-25 09:59:02.331','2021-12-25 09:59:02.331',1);

/*Table structure for table `comment_ratings` */

DROP TABLE IF EXISTS `comment_ratings`;

CREATE TABLE `comment_ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rating` double DEFAULT NULL,
  `rating_name` longtext,
  `article_comment_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_ratings_user` (`user_id`),
  KEY `fk_comment_ratings_article_comment` (`article_comment_id`),
  CONSTRAINT `fk_comment_ratings_article_comment` FOREIGN KEY (`article_comment_id`) REFERENCES `article_comments` (`id`),
  CONSTRAINT `fk_comment_ratings_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comment_ratings` */

/*Table structure for table `user_categories` */

DROP TABLE IF EXISTS `user_categories`;

CREATE TABLE `user_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext,
  `description` longtext,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user_categories` */

insert  into `user_categories`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'admin','admin deskripsi','2021-12-25 16:29:46.237','2021-12-25 16:29:46.237'),
(2,'writer','writer deskripsi','2021-12-25 16:30:11.296','2021-12-25 16:30:11.296'),
(3,'guest','guest deskripsi deskripsi','2021-12-25 16:30:34.897','2021-12-25 16:30:34.897');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` longtext NOT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_category_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_user_categories_user` (`user_category_id`),
  CONSTRAINT `fk_user_categories_user` FOREIGN KEY (`user_category_id`) REFERENCES `user_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`email`,`password`,`created_at`,`updated_at`,`user_category_id`) values 
(1,'erwineka99','erwineka99@gmail.com','$2a$10$/2v5wsBJmB5YibwjLXzoxuxaCwFItdUQvDXzL9Nei4LqtdzCvZKGW','2021-12-24 23:57:10.354','2021-12-26 23:06:33.901',1),
(2,'erwineka9957','erwineka9957@gmail.com','$2a$10$XtjTOdhXb9tJeYFkO33PzeCYc4IHrIUnUiBdJfzF.x4VWj8y5aX36','2021-12-26 22:42:01.069','2021-12-26 22:42:01.069',3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
