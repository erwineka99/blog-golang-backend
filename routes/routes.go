package routes

import (
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"

    "blog-golang-backend/controllers"
    "blog-golang-backend/middlewares"

    swaggerFiles "github.com/swaggo/files"     // swagger embed files
    ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
    r := gin.Default()
    
    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })


    //auth
    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)
    protected := r.Group("/user")
	protected.Use(middlewares.JwtAuthMiddleware())
	protected.GET("/current",controllers.CurrentUser)
    protected.PATCH("/current-password",controllers.UpdatePassword)


    r.GET("/articles", controllers.GetAllArticle)
    r.GET("/articles/:id", controllers.GetArticleById)

    articlesMiddlewareRoute := r.Group("/articles")
    articlesMiddlewareRoute.Use(middlewares.JwtAuthMiddlewareOnlyAdminAndWriter())
    //article only admin and writer
    articlesMiddlewareRoute.POST("/", controllers.CreateArticle)
    articlesMiddlewareRoute.PATCH("/:id", controllers.UpdateArticle)
    articlesMiddlewareRoute.DELETE("/:id", controllers.DeleteArticle)
    articlesMiddlewareRoute.GET("/log", controllers.GetAllArticleLogs)

    r.GET("/article-categories", controllers.GetAllArticleCategory)
    
    r.GET("/article-categories/:id/articles", controllers.GetArticlesByArticleCategoryId)

    //userCategory only admin can update, this is like maser data
    categoryArticleMiddlewareRoute := r.Group("/article-categories")
    
    categoryArticleMiddlewareRoute.Use(middlewares.JwtAuthMiddlewareOnlyAdmin())
    categoryArticleMiddlewareRoute.GET("/:id", controllers.GetArticleCategoryById)
    categoryArticleMiddlewareRoute.POST("/", controllers.CreateArticleCategory)
    categoryArticleMiddlewareRoute.PATCH("/:id", controllers.UpdateArticleCategory)
    categoryArticleMiddlewareRoute.DELETE("/:id", controllers.DeleteArticleCategory)



    //userCategory on admin can update, this is like maser data
  
    userCategoryMiddlewareRoute := r.Group("/user-categories")
    userCategoryMiddlewareRoute.Use(middlewares.JwtAuthMiddlewareOnlyAdmin())
    userCategoryMiddlewareRoute.GET("/", controllers.GetAllUserCategory)
    userCategoryMiddlewareRoute.POST("/", controllers.CreateUserCategory)
    userCategoryMiddlewareRoute.PATCH("/:id", controllers.UpdateUserCategory)
    userCategoryMiddlewareRoute.DELETE("/:id", controllers.DeleteUserCategory)

    //user only admin can update, this is like  master data
    userMiddlewareRoute := r.Group("/users")
    userMiddlewareRoute.Use(middlewares.JwtAuthMiddlewareOnlyAdmin())
    userMiddlewareRoute.GET("/", controllers.GetAllUser)
    userMiddlewareRoute.POST("/", controllers.CreateUser)
    userMiddlewareRoute.GET("/:id", controllers.GetUserById)
    userMiddlewareRoute.PATCH("/:id", controllers.UpdateUser)

    //article Rating Only Guest
    r.GET("/rating-articles/:id/articles", controllers.GetRatingByArticleId)
    articleRatingMiddlewareRoute := r.Group("/article-ratings")
    articleRatingMiddlewareRoute.Use(middlewares.JwtAuthMiddlewareOnlyGuest())
    articleRatingMiddlewareRoute.POST("/",controllers.CreateArticleRating)
    articleRatingMiddlewareRoute.PATCH("/:id",controllers.UpdateArticleRating)

    //comment Rating only Guest and Writer
    r.GET("/comment-ratings/:id/comments", controllers.GetRatingByCommentId)
    commentRatingMiddlewareRoute := r.Group("/comment-ratings")
    commentRatingMiddlewareRoute.Use(middlewares.JwtAuthMiddlewareOnlyGuestAndWriter())
    commentRatingMiddlewareRoute.POST("/",controllers.CreateCommentRating)
    commentRatingMiddlewareRoute.PATCH("/:id",controllers.UpdateCommentRating)


    //article Comments, can acces with admin, writer and guest
    r.GET("/article-comments/:id/article", controllers.GetArticlesByArticleCategoryId)
    articleCommentsMiddlewareRoute := r.Group("/article-comments")
    articleCommentsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    articleCommentsMiddlewareRoute.POST("/", controllers.CreateArticleComment)
    articleCommentsMiddlewareRoute.PATCH("/:id", controllers.UpdateArticleComment)
    articleCommentsMiddlewareRoute.DELETE("/:id", controllers.DeleteArticleComment)




    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

    return r
}