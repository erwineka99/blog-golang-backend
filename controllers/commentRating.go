package controllers

import (
    "net/http"
    "time"
    "fmt"

    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
)

type commentRatingInput struct {
    Rating              float64           `json:"rating"`
	ArticleCommentID   		uint          `json:"articleID"`
}


// GetRatingeByArticleCommentCategoryId godoc
// @Summary Get CommentRating. This is available for all user no need middleware.
// @Description Get all CommentRating by ArticleCommentID.
// @Tags CommentRating
// @Produce json
// @Param id path string true "Article Comment id"
// @Success 200 {object} []models.CommentRating
// @Router /comment-ratings/{id}/comments [get]
func GetRatingByCommentId(c *gin.Context) { // Get model if exist
    var comment_rating []models.ArticleRating

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("article_comment_id = ?", c.Param("id")).Find(&comment_rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": comment_rating})
}


// CreateCommentRating godoc
// @Summary Create New CreateCommentRating.
// @Description Creating a new CreateCommentRating. This can acces for writer, guest. Admin Can not acces this so admin can not manipualte rating.
// @Tags CommentRating
// @Param Body body commentRatingInput true "the body to create a new CreateCommentRating"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.CommentRating
// @Router /comment-ratings [post]
func CreateCommentRating(c *gin.Context) {
    // Validate input
    var input commentRatingInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)

	var ratingAngka float64
	
	ratingAngka = input.Rating
	var ratingHuruf string

	if ratingAngka>4 {
		ratingHuruf="Awesome"
	}else if ratingAngka>3 {
		ratingHuruf="Very Good"
	}else if ratingAngka>2 {
		ratingHuruf="Good"
	}else if ratingAngka>1 {
		ratingHuruf="Decent"
	}else{
		ratingHuruf="Bad"
	}

    // Create Rating
    comment_rating := models.CommentRating{Rating: input.Rating, ArticleCommentID: input.ArticleCommentID,RatingName:ratingHuruf,UserID:user_id}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&comment_rating)

    c.JSON(http.StatusOK, gin.H{"data": comment_rating})
}


// UpdateCommentRating godoc
// @Summary Update CommentRating.
// @Description Update CommentRating by id. This can acces for writer, guest. Admin Can not acces this so admin can not manipualte rating.
// @Tags CommentRating
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "CommentRating id"
// @Param Body body userCategoryInput true "the body to update age rating category"
// @Success 200 {object} models.CommentRating
// @Router /comment-ratings/{id} [patch]
func UpdateCommentRating(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var comment_rating models.CommentRating
    if err := db.Where("id = ?", c.Param("id")).First(&comment_rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input commentRatingInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }


	var ratingAngka float64
	
	ratingAngka = input.Rating
	var ratingHuruf string

	if ratingAngka>4 {
		ratingHuruf="Awesome"
	}else if ratingAngka>3 {
		ratingHuruf="Very Good"
	}else if ratingAngka>2 {
		ratingHuruf="Good"
	}else if ratingAngka>1 {
		ratingHuruf="Decent"
	}else{
		ratingHuruf="Bad"
	}

    var updatedInput models.CommentRating
    updatedInput.Rating = input.Rating
    updatedInput.RatingName = ratingHuruf
    updatedInput.UpdatedAt = time.Now()

    db.Model(&comment_rating).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": comment_rating})
}