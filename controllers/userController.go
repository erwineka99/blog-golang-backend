

package controllers

import (
    "net/http"
    "time"
    "fmt"
    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
    "golang.org/x/crypto/bcrypt"
)

type userInput struct {
    Username            string `json:"title"`
    Email             	string  `json:"content"`
    Password   			string   `json:"article_category_id"`
	UserCategoryID      uint    `json:"user_category_id"`
}

// GetAllUsers godoc
// @Summary Get all users.
// @Description Get a list of Users.  This is only for admin. Can using on dashboard management.
// @Tags User
// @Param Body body articleInput true "the body to create a new Article"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Produce json
// @Success 200 {object} []models.User
// @Router /users [get]
func GetAllUser(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var users []models.User
    db.Find(&users)

    c.JSON(http.StatusOK, gin.H{"data": users})
}

// CreateUser godoc
// @Summary Create New User.
// @Description Creating a new User. This is only for admin. Can using on dashboard management.
// @Tags User
// @Param Body body userInput true "the body to create a new User"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.User
// @Router /users [post]
func CreateUser(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input userInput
    var userCategory models.UserCategory
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserCategoryID).First(&userCategory).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserCategoryID not found!"})
        return
    }
    
    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
    hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
    // Create User
    user := models.User{Username: input.Username, Email: input.Email, Password: string(hashedPassword),UserCategoryID:input.UserCategoryID}
    db.Create(&user)

    c.JSON(http.StatusOK, gin.H{"data": user})
}

// GetUserById godoc
// @Summary Get User.
// @Description Get a User by id. This is only for admin. Can using on dashboard management. This is get data before edit data.
// @Tags User
// @Param Body body userInput true "the body to create a new User"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Produce json
// @Param id path string true "user id"
// @Success 200 {object} models.User
// @Router /users/{id} [get]
func GetUserById(c *gin.Context) { // Get model if exist
    var user models.User

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": user})
}

// UpdateUser godoc
// @Summary Update User.
// @Description Update user by id. This is only for admin. Can using on dashboard management.
// @Tags User
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "user id"
// @Param Body body articleInput true "the body to update an user"
// @Success 200 {object} models.User
// @Router /users/{id} [patch]
func UpdateUser(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var user models.User
    var user_category models.UserCategory
    if err := db.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input userInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserCategoryID).First(&user_category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserCategoryID not found!"})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)

    hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)

    var updatedInput models.User
    updatedInput.Username = input.Username
    updatedInput.Email = input.Email
    updatedInput.Password = string(hashedPassword)
	updatedInput.UserCategoryID = input.UserCategoryID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&user).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": user})
}