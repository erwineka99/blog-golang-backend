package controllers

import (
    "net/http"
    "time"
    "fmt"

    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
)

type articleRatingInput struct {
    Rating              float64           `json:"rating"`
	ArticleID   		uint              `json:"articleID"`
}


// GetArticleByArticleCategoryId godoc
// @Summary Get Rating.
// @Description Get all Rating by ArticleID. This is can acces for ALL
// @Tags ArticleRating
// @Produce json
// @Param id path string true "ArticleRating id"
// @Success 200 {object} []models.ArticleRating
// @Router /article-rating/{id}/articles [get]
func GetRatingByArticleId(c *gin.Context) { // Get model if exist
    var article_rating []models.ArticleRating

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("article_id = ?", c.Param("id")).Find(&article_rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": article_rating})
}


// CreateArticleRating godoc
// @Summary Create New CreateArticleRating.
// @Description Creating a new CreateArticleRating. This can acces for guest. Admin and writer Can not acces this so admin can not manipualte rating.
// @Tags ArticleRating
// @Param Body body articleRatingInput true "the body to create a new CreateArticleRating"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.ArticleRating
// @Router /article-ratings [post]
func CreateArticleRating(c *gin.Context) {
    // Validate input
    var input articleRatingInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)

	var ratingAngka float64
	
	ratingAngka = input.Rating
	var ratingHuruf string

	if ratingAngka>4 {
		ratingHuruf="Awesome"
	}else if ratingAngka>3 {
		ratingHuruf="Very Good"
	}else if ratingAngka>2 {
		ratingHuruf="Good"
	}else if ratingAngka>1 {
		ratingHuruf="Decent"
	}else{
		ratingHuruf="Bad"
	}

    // Create Rating
    article_rating := models.ArticleRating{Rating: input.Rating, ArticleID: input.ArticleID,RatingName:ratingHuruf,UserID:user_id}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&article_rating)

    c.JSON(http.StatusOK, gin.H{"data": article_rating})
}


// UpdateArticleRating godoc
// @Summary Update ArticleRating.
// @Description Update ArticleRating by id.  This can acces for guest. Admin and writer Can not acces this so admin can not manipualte rating.
// @Tags ArticleRating
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "ArticleRating id"
// @Param Body body userCategoryInput true "the body to update age rating category"
// @Success 200 {object} models.ArticleRating
// @Router /article-ratings/{id} [patch]
func UpdateArticleRating(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var article_rating models.ArticleRating
    if err := db.Where("id = ?", c.Param("id")).First(&article_rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input articleRatingInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }


	var ratingAngka float64
	
	ratingAngka = input.Rating
	var ratingHuruf string

	if ratingAngka>4 {
		ratingHuruf="Awesome"
	}else if ratingAngka>3 {
		ratingHuruf="Very Good"
	}else if ratingAngka>2 {
		ratingHuruf="Good"
	}else if ratingAngka>1 {
		ratingHuruf="Decent"
	}else{
		ratingHuruf="Bad"
	}

    var updatedInput models.ArticleRating
    updatedInput.Rating = input.Rating
    updatedInput.RatingName = ratingHuruf
    updatedInput.UpdatedAt = time.Now()

    db.Model(&article_rating).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": article_rating})
}