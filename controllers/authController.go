package controllers

import (
    "blog-golang-backend/models"
    "fmt"
    "net/http"
    "time"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
    "golang.org/x/crypto/bcrypt"
)


type LoginInput struct {
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
}

type RegisterInput struct {
    Username string `json:"username" binding:"required"`
    Password string `json:"password" binding:"required"`
    Email    string `json:"email" binding:"required"`
    UserCategoryID uint `json:"user_category_id"`
}

type PasswordInput struct {
    Password string `json:"password" binding:"required"`
}


// LoginUser godoc
// @Summary Login as as user.
// @Description Logging in to get jwt token to access admin or user api by roles.
// @Tags Auth
// @Param Body body LoginInput true "the body to login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input LoginInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

    u.Username = input.Username
    u.Password = input.Password
    u.UserCategoryID = 3

    token, err := models.LoginCheck(u.Username, u.Password, db)

    if err != nil {
        fmt.Println(err)
        c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
        return
    }

    user := map[string]string{
        "username": u.Username,
        "email":    u.Email,
    }

    c.JSON(http.StatusOK, gin.H{"message": "login success", "user": user, "token": token})

}

// Register godoc
// @Summary Register a user.
// @Description registering a user from public access. For testing use we can input user category 1 for admin, 2 writer, 3 guest
// @Tags Auth
// @Param Body body RegisterInput true "the body to register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input RegisterInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

    u.Username = input.Username
    u.Email = input.Email
    u.Password = input.Password
    u.UserCategoryID = input.UserCategoryID 

    var  user_category_string string
    if input.UserCategoryID ==1 || input.UserCategoryID==0{
        user_category_string="admin"
    }else if  input.UserCategoryID ==2 {
        user_category_string="writer"
    }else{
        user_category_string="writer"
    }

    _, err := u.SaveUser(db)

    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    //user_category_id default 3 by guest if user category not filled
    user := map[string]string{
        "username": input.Username,
        "email":    input.Email,
        "user_category " : user_category_string,
    }

    c.JSON(http.StatusOK, gin.H{"message": "registration success", "user": user})

}



// GetCurrentUser godoc
// @Summary Get current user.
// @Description This For Get Data For My Profile, But for update data we can use /update/user id.
// @Tags Auth
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.User
// @Router /user/current [get]
func CurrentUser(c *gin.Context){
    db := c.MustGet("db").(*gorm.DB)
	user_id, err := token.ExtractTokenID(c)
	
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
	
	u,err := models.GetUserByID(user_id,db)
	
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message":"success","data":u})
}

// UpdateUser godoc
// @Summary Update Password Current User.
// @Description Update Password Current User using Bearer token ID, like current user.
// @Tags Auth
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param Body body PasswordInput true "the body to update password"
// @Success 200 {object} models.User
// @Router /user/current-password/ [patch]
func UpdatePassword(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)


    user_id, err := token.ExtractTokenID(c)
	
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("current_user id", user_id)



    // Get model if exist
    var user models.User
    if err := db.Where("id = ?", user_id).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input PasswordInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)

    var updatedInput models.User
    updatedInput.Password = string(hashedPassword)
    updatedInput.UpdatedAt = time.Now()

    db.Model(&user).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": "update password succes"})
}