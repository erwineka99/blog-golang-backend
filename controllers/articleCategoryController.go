package controllers

import (
    "net/http"
    "time"
    "fmt"

    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
)

type articleCategoryInput struct {
    Name        string `json:"name"`
    Description string `json:"description"`
}

// GetAllCategoryArticle godoc
// @Summary Get all ArticleCategory.
// @Description Get a list of ArticleCategory, there is no middleware on here.
// @Tags ArticleCategory
// @Produce json
// @Success 200 {object} []models.ArticleCategory
// @Router /article-categories [get]
func GetAllArticleCategory(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var article_category []models.ArticleCategory
    db.Find(&article_category)

    c.JSON(http.StatusOK, gin.H{"data": article_category})
}

// GetArticleByArticleCategoryId godoc
// @Summary Get Articles filter by category id.
// @Description Get all Articles by ArticleCategoryId, this is for filter. Sometimes there is category link on right menu, no need middleware.
// @Tags ArticleCategory
// @Produce json
// @Param id path string true "ArticleCategory id"
// @Success 200 {object} []models.Article
// @Router /article-categories/{id}/articles [get]
func GetArticlesByArticleCategoryId(c *gin.Context) { // Get model if exist
    var articles []models.Article

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("article_category_id = ?", c.Param("id")).Find(&articles).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": articles})
}


// CreateArticleCategory godoc
// @Summary Create New ArticleCategory.
// @Description Creating a new ArticleCategory, this is only for admin. Can using on dashboard management.
// @Tags ArticleCategory
// @Param Body body articleCategoryInput true "the body to create a new ArticleCategory"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.ArticleCategory
// @Router /article-categories [post]
func CreateArticleCategory(c *gin.Context) {
    // Validate input
    var input articleCategoryInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
    // Create Rating
    category := models.ArticleCategory{Name: input.Name, Description: input.Description}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&category)

    c.JSON(http.StatusOK, gin.H{"data": category})
}

// GetArticleCategoryById godoc
// @Summary Get ArticleCategory.
// @Description Get an ArticleCategory by id, this is only for admin. Can using on dashboard management, when get data before edit data.
// @Tags ArticleCategory
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "ArticleCategory id"
// @Success 200 {object} models.ArticleCategory
// @Router /article-categories/{id} [get]
func GetArticleCategoryById(c *gin.Context) { // Get model if exist
    var category models.ArticleCategory

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": category})
}


// UpdateArticleCategory godoc
// @Summary Update ArticleCategory.
// @Description Update ArticleCategory by id,  this is only for admin. Can using on dashboard management.
// @Tags ArticleCategory
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "ArticleCategory id"
// @Param Body body articleCategoryInput true "the body to update age rating category"
// @Success 200 {object} models.ArticleCategory
// @Router /article-categories/{id} [patch]
func UpdateArticleCategory(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var category models.ArticleCategory
    if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input articleCategoryInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.ArticleCategory
    updatedInput.Name = input.Name
    updatedInput.Description = input.Description
    updatedInput.UpdatedAt = time.Now()

    db.Model(&category).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": category})
}

// DeleteArticleCategory godoc
// @Summary Delete one ArticleCategory,  this is only for admin. Can using on dashboard management.
// @Description Delete a ArticleCategory by id.
// @Tags ArticleCategory
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "ArticleCategory id"
// @Success 200 {object} map[string]boolean
// @Router /article-categories/{id} [delete]
func DeleteArticleCategory(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var category models.ArticleCategory
    if err := db.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&category)

    c.JSON(http.StatusOK, gin.H{"data": true})
}