

package controllers

import (
    "net/http"
    "time"
    "fmt"
    "encoding/json"
    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
)

type articleInput struct {
    Title               string `json:"title"`
    Content             string  `json:"content"`
    ArticleCategoryID   uint   `json:"article_category_id"`
}

// GetAllArticles godoc
// @Summary Get all articles.
// @Description Get a list of Articles. This is can acces for public
// @Tags Article
// @Produce json
// @Success 200 {object} []models.Article
// @Router /articles [get]
func GetAllArticle(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var articles []models.Article
    db.Find(&articles)

    c.JSON(http.StatusOK, gin.H{"data": articles})
}

// CreateArticle godoc
// @Summary Create New Article.
// @Description Creating a new Article. This can acces for admin and writers.
// @Tags Article
// @Param Body body articleInput true "the body to create a new Article"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Article
// @Router /articles [post]
func CreateArticle(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input articleInput
    var category models.ArticleCategory
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.ArticleCategoryID).First(&category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ArticleCategoryID not found!"})
        return
    }
    
    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
    // Create Movie
    article := models.Article{Title: input.Title, Content: input.Content, ArticleCategoryID: input.ArticleCategoryID,UserID:user_id}
    db.Create(&article)

    c.JSON(http.StatusOK, gin.H{"data": article})
}

// GetArticleById godoc
// @Summary Get Article.
// @Description Get a Article by id.  This is can acces for public for detail
// @Tags Article
// @Produce json
// @Param id path string true "article id"
// @Success 200 {object} models.Article
// @Router /articles/{id} [get]
func GetArticleById(c *gin.Context) { // Get model if exist
    var article models.Article

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&article).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": article})
}

// UpdateArticle godoc
// @Summary Update Article.
// @Description Update article by id. This can acces for admin and writers.
// @Tags Article
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "article id"
// @Param Body body articleInput true "the body to update an article"
// @Success 200 {object} models.Article
// @Router /articles/{id} [patch]
func UpdateArticle(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var article models.Article
    var category models.ArticleCategory
    if err := db.Where("id = ?", c.Param("id")).First(&article).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input articleInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.ArticleCategoryID).First(&category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ArticleCategoryID not found!"})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)


    var updatedInput models.Article
    updatedInput.Title = input.Title
    updatedInput.Content = input.Content
    updatedInput.ArticleCategoryID = input.ArticleCategoryID
    updatedInput.UpdatedAt = time.Now()
    db.Model(&article).Updates(updatedInput)


    //addArticleLogs
    var articleLog models.ArticleLog
    data, _ := json.Marshal(article)
    articleLog = models.ArticleLog{Action: "Update Article", ArticleID: article.ID, UserID:user_id,LogAt:time.Now(),OldArticle:string(data)}
    db.Create(&articleLog)

    c.JSON(http.StatusOK, gin.H{"data": article})
}

// DeleteArticle godoc
// @Summary Delete one article.
// @Description Delete a article by id. This can acces for admin and writers.
// @Tags Article
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "article id"
// @Success 200 {object} map[string]boolean
// @Router /articles/{id} [delete]
func DeleteArticle(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var article models.Article
    if err := db.Where("id = ?", c.Param("id")).First(&article).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
    var ArticleIDTemp = article.ID


    data, _ := json.Marshal(article)
  
    db.Delete(&article)

    var articleLog models.ArticleLog
    articleLog = models.ArticleLog{Action: "Delete Article", ArticleID: ArticleIDTemp, UserID:user_id,LogAt:time.Now(),OldArticle:string(data)}
    db.Create(&articleLog)


    c.JSON(http.StatusOK, gin.H{"data": true})
}