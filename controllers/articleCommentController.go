

package controllers

import (
    "net/http"
    "time"
    "fmt"
    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
)

type articleCommentInput struct {
    Comment               string  `json:"Comment"`
    ParentCommentID       uint    `json:"ParentCommentID"`
    ArticleID     		  uint    `json:"ArticleID"`
}

// GetCommentsByArticleID godoc
// @Summary Get all comments by article id.
// @Description Get a list comments by article id. This can open for admin, writer, guest. Admin using this for dashboard management.
// @Tags ArticleComment
// @Produce json
// @Param id path string true "Article ID"
// @Success 200 {object} []models.ArticleComment
// @Router /article-comments/{id}/article [get]
func GetCommentByArticleID(c *gin.Context) {
    var article_comment []models.ArticleComment

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("article_id = ?", c.Param("id")).Find(&article_comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": article_comment})
}

// CreateArticleComment godoc
// @Summary Create New ArticleComment.
// @Description Creating a new ArticleComment. This can open for admin, writer, guest. Admin using this for dashboard management.
// @Tags ArticleComment
// @Param Body body articleInput true "the body to create a new ArticleComment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.ArticleComment
// @Router /article-comments [post]
func CreateArticleComment(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input articleCommentInput
    var article models.Article
	var comment models.ArticleComment
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.ArticleID).First(&article).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ArticleID not found!"})
        return
    }

	if err := db.Where("id = ?", input.ParentCommentID).First(&comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ParentCommentID not found!"})
        return
    }
    
    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
    // Create Movie
    comment_create := models.ArticleComment{Comment: input.Comment, ArticleID: input.ArticleID, ParentCommentID: input.ParentCommentID,UserID:user_id}
    db.Create(&comment_create)

    c.JSON(http.StatusOK, gin.H{"data": comment_create})
}


// UpdateArticleComment godoc
// @Summary Update ArticleComment.
// @Description Update articleComment by id. This can open for admin, writer, guest. Admin using this for dashboard management.
// @Tags ArticleComment
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "ArticleComment id"
// @Param Body body articleCommentInput true "the body to update an ArticleComment"
// @Success 200 {object} models.ArticleComment 
// @Router /article-comments/{id} [patch]
func UpdateArticleComment(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input articleCommentInput
    var article models.Article
	var comment models.ArticleComment
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.ArticleID).First(&article).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ArticleID not found!"})
        return
    }

	if err := db.Where("id = ?", input.ParentCommentID).First(&comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "ParentCommentID not found!"})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)


    var updatedInput models.ArticleComment
    updatedInput.Comment = input.Comment
    updatedInput.UpdatedAt = time.Now()
    db.Model(&comment).Updates(updatedInput)


    c.JSON(http.StatusOK, gin.H{"data": comment})
}

// DeleteArticleComment godoc
// @Summary Delete one article.
// @Description Delete a article by id. This can open for admin, writer, guest. Admin using this for dashboard management.
// @Tags ArticleComment
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "article comment id"
// @Success 200 {object} map[string]boolean
// @Router /article-comments/{id} [delete]
func DeleteArticleComment(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var article_comment models.ArticleComment
    if err := db.Where("id = ?", c.Param("id")).First(&article_comment).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

	db.Delete(&article_comment)


    c.JSON(http.StatusOK, gin.H{"data": true})
}