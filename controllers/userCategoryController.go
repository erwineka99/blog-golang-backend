package controllers

import (
    "net/http"
    "time"
    "fmt"

    "blog-golang-backend/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "blog-golang-backend/utils/token"
)

type userCategoryInput struct {
    Name        string `json:"name"`
    Description string `json:"description"`
}

// GetAllCategoryUser godoc
// @Summary Get all UserCategory.
// @Description Get a list of UserCategory. This is only for admin. Can using on dashboard management.
// @Tags UserCategory
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.UserCategory
// @Router /user-categories [get]
func GetAllUserCategory(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var user_category []models.UserCategory
    db.Find(&user_category)

    c.JSON(http.StatusOK, gin.H{"data": user_category})
}

// CreateUserCategory godoc
// @Summary Create New UserCategory.
// @Description Creating a new UserCategory. This is only for admin. Can using on dashboard management.
// @Tags UserCategory
// @Param Body body userCategoryInput true "the body to create a new UserCategory"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.UserCategory
// @Router /user-categories [post]
func CreateUserCategory(c *gin.Context) {
    // Validate input
    var input userCategoryInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user_id, err := token.ExtractTokenID(c)
    if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
    fmt.Println("user id", user_id)
    // Create Rating
    user_category := models.UserCategory{Name: input.Name, Description: input.Description}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&user_category)

    c.JSON(http.StatusOK, gin.H{"data": user_category})
}


// UpdateUserCategory godoc
// @Summary Update UserCategory.
// @Description Update UserCategory by id. This is only for admin. Can using on dashboard management.
// @Tags UserCategory
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "UserCategory id"
// @Param Body body userCategoryInput true "the body to update age rating category"
// @Success 200 {object} models.UserCategory
// @Router /user-categories/{id} [patch]
func UpdateUserCategory(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var user_category models.UserCategory
    if err := db.Where("id = ?", c.Param("id")).First(&user_category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input userCategoryInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.UserCategory
    updatedInput.Name = input.Name
    updatedInput.Description = input.Description
    updatedInput.UpdatedAt = time.Now()

    db.Model(&user_category).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": user_category})
}



// DeleteUserCategory godoc
// @Summary Delete one UserCategory.
// @Description Delete a UserCategory by id. This is only for admin. Can using on dashboard management.
// @Tags UserCategory
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "UserCategory id"
// @Success 200 {object} map[string]boolean
// @Router /user-categories/{id} [delete]
func DeleteUserCategory(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var user_category models.UserCategory
    if err := db.Where("id = ?", c.Param("id")).First(&user_category).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&user_category)

    c.JSON(http.StatusOK, gin.H{"data": true})
}