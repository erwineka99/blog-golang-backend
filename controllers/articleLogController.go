package controllers

import (
    "blog-golang-backend/models"
    "net/http"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

// GetAllArticleLogs godoc
// @Summary Get all articles log.
// @Description Get a list of Articles Log. This can acces for admin and writers.
// @Tags ArticleLog
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.ArticleLog
// @Router /articles/log [get]
func GetAllArticleLogs(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var article_logs []models.ArticleLog
    db.Find(&article_logs)

    c.JSON(http.StatusOK, gin.H{"data": article_logs})
}
