package middlewares

import (
    "net/http"

    "blog-golang-backend/utils/token"

    "github.com/gin-gonic/gin"
    "blog-golang-backend/models"
    "fmt"
    "gorm.io/gorm"
)

func JwtAuthMiddleware() gin.HandlerFunc {
    return func(c *gin.Context) {
        err := token.TokenValid(c)
        if err != nil {
            
            c.String(http.StatusUnauthorized, err.Error())
            c.Abort()
            return
        }
        c.Next()
    }
}

func JwtAuthMiddlewareOnlyGuest() gin.HandlerFunc {
    return func(c *gin.Context) {
        err := token.TokenValid(c)
        if err != nil {
            
            c.String(http.StatusUnauthorized, err.Error())
            c.Abort()
            return
        }
        //check user role
        db := c.MustGet("db").(*gorm.DB)
        user_id, err := token.ExtractTokenID(c)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        fmt.Println("user id", user_id)
        
        u,err := models.GetUserByID(user_id,db)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        var next = true
        if(u.ID>0){
            if(u.UserCategoryID !=3){
                next=false
                c.JSON(http.StatusBadRequest, gin.H{"error": "Only Guest Can Acces This API"})
                c.Abort()
                return
            }
        }
        if next {
            c.Next()
        }      
    }
}

func JwtAuthMiddlewareOnlyAdmin() gin.HandlerFunc {
    return func(c *gin.Context) {
        err := token.TokenValid(c)
        if err != nil {
            
            c.String(http.StatusUnauthorized, err.Error())
            c.Abort()
            return
        }
        //check user role
        db := c.MustGet("db").(*gorm.DB)
        user_id, err := token.ExtractTokenID(c)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        fmt.Println("user id", user_id)
        
        u,err := models.GetUserByID(user_id,db)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        var next = true
        if(u.ID>=0){
            if(u.UserCategoryID !=1){
                next=false
                c.JSON(http.StatusBadRequest, gin.H{"error": "Only Admin Can Acces This API"})
                c.Abort()
                return
            }
        }
        if next {
            c.Next()
        }      
    }
}



func JwtAuthMiddlewareOnlyWriter() gin.HandlerFunc {
    return func(c *gin.Context) {
        err := token.TokenValid(c)
        if err != nil {
            
            c.String(http.StatusUnauthorized, err.Error())
            c.Abort()
            return
        }
        //check user role
        db := c.MustGet("db").(*gorm.DB)
        user_id, err := token.ExtractTokenID(c)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        fmt.Println("user id", user_id)
        
        u,err := models.GetUserByID(user_id,db)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        var next = true
        if(u.ID>0){
            if(u.UserCategoryID !=2){
                next=false
                c.JSON(http.StatusBadRequest, gin.H{"error": "Only Admin Can Acces This API"})
                c.Abort()
                return
            }
        }
        if next {
            c.Next()
        }      
    }
}


func JwtAuthMiddlewareOnlyAdminAndWriter() gin.HandlerFunc {
    return func(c *gin.Context) {
        err := token.TokenValid(c)
        if err != nil {
            
            c.String(http.StatusUnauthorized, err.Error())
            c.Abort()
            return
        }
        //check user role
        db := c.MustGet("db").(*gorm.DB)
        user_id, err := token.ExtractTokenID(c)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        fmt.Println("user id", user_id)
        
        u,err := models.GetUserByID(user_id,db)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        var next = true
        if(u.ID>=0){
            if(u.UserCategoryID !=2 || u.UserCategoryID !=1){
                next=false
                c.JSON(http.StatusBadRequest, gin.H{"error": "Only Admin and Writer Can Acces This API"})
                c.Abort()
                return
            }
        }
        if next {
            c.Next()
        }      
    }
}



func JwtAuthMiddlewareOnlyGuestAndWriter() gin.HandlerFunc {
    return func(c *gin.Context) {
        err := token.TokenValid(c)
        if err != nil {
            
            c.String(http.StatusUnauthorized, err.Error())
            c.Abort()
            return
        }
        //check user role
        db := c.MustGet("db").(*gorm.DB)
        user_id, err := token.ExtractTokenID(c)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        fmt.Println("user id", user_id)
        
        u,err := models.GetUserByID(user_id,db)
        
        if err != nil {
            c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
            return
        }
        var next = true
        if(u.ID>0){
            if(u.UserCategoryID !=2 || u.UserCategoryID !=3){
                next=false
                c.JSON(http.StatusBadRequest, gin.H{"error": "Only Admin and Writer Can Acces This API"})
                c.Abort()
                return
            }
        }
        if next {
            c.Next()
        }      
    }
}