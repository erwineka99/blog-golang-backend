package models

import (
    "time"
)

type (
    // Movie
    Article struct {
        ID                  uint              `json:"id" gorm:"primary_key"`
        Title               string            `json:"title"`
        Content             string            `json:"content"`
        ArticleCategoryID   uint              `json:"articleCategoryID"`
        UserID              uint              `json:"userID"`
        CreatedAt           time.Time         `json:"created_at"`
        UpdatedAt           time.Time         `json:"updated_at"`
        ArticleCategory     ArticleCategory   `json:"-"`
        User                User              `json:"-"`
    }
)