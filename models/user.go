package models

import (
    "html"
    "strings"
    "time"
    "errors"

    "blog-golang-backend/utils/token"

    "golang.org/x/crypto/bcrypt"
    "gorm.io/gorm"
)

type (
    // User
    User struct {
        ID              uint            `json:"id" gorm:"primary_key"`
        Username        string          `gorm:"not null;unique" json:"username"`
        Email           string          `gorm:"not null;unique" json:"email"`
        Password        string          `gorm:"not null;" json:"password"`
        UserCategoryID  uint            `json:"user_category_id"`
        CreatedAt       time.Time       `json:"created_at"`
        UpdatedAt       time.Time       `json:"updated_at"`
        UserCategory    UserCategory    `json:"-"`
    }
)

func VerifyPassword(password, hashedPassword string) error {
    return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func LoginCheck(username string, password string, db *gorm.DB) (string, error) {

    var err error

    u := User{}

    err = db.Model(User{}).Where("username = ?", username).Take(&u).Error

    if err != nil {
        return "", err
    }

    err = VerifyPassword(password, u.Password)

    if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
        return "", err
    }

    token, err := token.GenerateToken(u.ID)

    if err != nil {
        return "", err
    }

    return token, nil

}

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
    //turn password into hash
    hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
    if errPassword != nil {
        return &User{}, errPassword
    }
    u.Password = string(hashedPassword)
    //remove spaces in username
    u.Username = html.EscapeString(strings.TrimSpace(u.Username))

    var err error = db.Create(&u).Error
    if err != nil {
        return &User{}, err
    }
    return u, nil
}

func GetUserByID(uid uint,db *gorm.DB) (User,error) {

	var u User

	if err := db.First(&u,uid).Error; err != nil {
		return u,errors.New("User not found!")
	}

	u.PrepareGive()
	
	return u,nil

}

func (u *User) PrepareGive(){
	u.Password = ""
}