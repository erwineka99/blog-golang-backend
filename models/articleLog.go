package models

import (
    "time"
)

type (
    // Article
    ArticleLog struct {
        ID                  uint              `json:"id" gorm:"primary_key"`
        Action              string            `json:"action"`
		OldArticle          string            `json:"old_article"`
        ArticleID   		uint              `json:"articleID"`
        UserID              uint              `json:"userID"`
        LogAt               time.Time         `json:"log_at"`
        User                User              `json:"-"`
        Article             Article           `json:"-"`
    }
)