package models

import (
    "time"
)

type (
    // Article
    ArticleComment struct {
        ID                  uint              `json:"id" gorm:"primary_key"`
        Comment             string            `json:"comment"`
		ParentCommentID     uint 			  `json:"parent_comment_id"`
        ArticleID   		uint              `json:"articleID"`
        UserID              uint              `json:"userID"`
		CreatedAt   		time.Time 		  `json:"created_at"`
        UpdatedAt   		time.Time 		  `json:"updated_at"`
        User                User              `json:"-"`
		Article             Article           `json:"-"`
    }
)