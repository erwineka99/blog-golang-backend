package models

import (
    "time"
)

type (
    // Article
    ArticleRating struct {
        ID                  uint              `json:"id" gorm:"primary_key"`
        Rating              float64            `json:"rating"`
		RatingName			string 			   `json:"title"`
        ArticleID   		uint              `json:"articleID"`
        UserID              uint              `json:"userID"`
		CreatedAt   		time.Time 		   `json:"created_at"`
        UpdatedAt   		time.Time 		  `json:"updated_at"`
        User                User              `json:"-"`
        Article             Article            `json:"-"`
    }
)