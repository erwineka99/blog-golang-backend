package models

import (
    "time"
)

type (
    // Comment Rating
    CommentRating struct {
        ID                  uint              `json:"id" gorm:"primary_key"`
        Rating              float64           `json:"rating"`
		RatingName			string 			  `json:"title"`
        ArticleCommentID   uint               `json:"ArticleCommentID"`
        UserID              uint              `json:"userID"`
		CreatedAt   		time.Time 		  `json:"created_at"`
        UpdatedAt   		time.Time 		  `json:"updated_at"`
        User                User              `json:"-"`
        ArticleComment      ArticleComment    `json:"-"`
    }
)