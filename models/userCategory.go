package models

import (
    "time"
)

type (
    // userCategory
    UserCategory struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        Name        string    `json:"name"`
        Description string    `json:"description"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
		User        []User    `json:"-"`
    }
)